<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Movie;

class MovieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $movie = new Movie();
        $movie->setTitle('Zielona mila');
        $movie->setCast('Tom Hanks, David Morse');
        $movie->setYear(1999);
        $movie->setCategory('Dramat');
        $movie->setDirector('Frank Darabont');
        $movie->setStory('Emerytowany strażnik więzienny opowiada przyjaciółce o niezwykłym mężczyźnie, którego skazano na śmierć za zabójstwo dwóch 9-letnich dziewczynek.');
        $movie->setPrice(29.99);
        $manager->persist($movie);

        $movie2 = new Movie();
        $movie2->setTitle('Skazani na Shawshank');
        $movie2->setCast('Tim Robbins, Morgan Freeman');
        $movie2->setYear(1994);
        $movie2->setCategory('Dramat');
        $movie2->setDirector('Frank Darabont');
        $movie2->setStory('Adaptacja opowiadania Stephena Kinga. Niesłusznie skazany na dożywocie bankier, stara się przetrwać w brutalnym, więziennym świecie.');
        $movie2->setPrice(49.99);
        $manager->persist($movie2);

        $movie3 = new Movie();
        $movie3->setTitle('Matrix');
        $movie3->setCast('Keanu Reeves, Laurence Fishburne');
        $movie3->setYear(1999);
        $movie3->setCategory('Akcja');
        $movie3->setDirector('Lilly Wachowski');
        $movie3->setStory('Haker komputerowy Neo dowiaduje się od tajemniczych rebeliantów, że świat, w którym żyje, jest tylko obrazem przesyłanym do jego mózgu przez roboty.');
        $movie3->setPrice(19.99);
        $manager->persist($movie3);

        $movie4 = new Movie();
        $movie4->setTitle('Milczenie owiec');
        $movie4->setCast('Anthony HopkinsJodie Foster');
        $movie4->setYear(1991);
        $movie4->setCategory('Thriller');
        $movie4->setDirector('Jonathan Demme');
        $movie4->setStory('Seryjny morderca i inteligentna agentka łączą siły, by znaleźć przestępcę obdzierającego ze skóry swoje ofiary.');
        $movie4->setPrice(39.99);
        $manager->persist($movie4);


        $manager->flush();
    }
}
