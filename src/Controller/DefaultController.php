<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Movie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends AbstractController
{
    public function index()
    {
        return $this->render('base.html.twig', [
            'cart' => $this->getCartMovies(),
            'movies' => $this->getAllMovies(),
            'categories' => $this->getAllMoviesCategories(),
            'current_category' => '',
            'zl' => $this->cartSum($this->getCartMovies()),
            'euro' => $this->moneyConverter($this->cartSum($this->getCartMovies()))
        ]);
    }

    public function category(Request $request)
    {
        return $this->render('base.html.twig', [
            'cart' => $this->getCartMovies(),
            'movies' => $this->getMoviesByCategory($request),
            'categories' => $this->getAllMoviesCategories(),
            'current_category' => $request->attributes->get('category'),
            'zl' => $this->cartSum($this->getCartMovies()),
            'euro' => $this->moneyConverter($this->cartSum($this->getCartMovies()))
        ]);
    }
    public function movie(Request $request)
    {
        return $this->render('movie.html.twig', [
            'movie' => $this->getMovie($request)
        ]);
    }

    public function thank()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $user->setCart([]);
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->render('thank.html.twig');
    }

    private function getMovie(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);
        return $repository->find($request->attributes->get('id'));
    }

    private function getAllMovies(): array
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);
        return $repository->findAll();
    }

    private function getMoviesByCategory(Request $request): array
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);
        return $repository->findBy(
            ['category' => $request->attributes->get('category')]
        );
    }

    public function getAllMoviesCategories(): array
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->createQuery(
            'SELECT  m.category
            FROM App\Entity\Movie AS m
            ORDER BY m.category ASC'
        );
        return array_unique(array_column($query->getArrayResult(), 'category'));
    }

    public function add(Request $request): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $cart = $user->getCart();
        $cart[] = $request->attributes->get('id');
        $user->setCart(array_unique($cart));
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute("index");
    }

    public function remove(Request $request): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $cart = $user->getCart();
        $cart = array_diff($cart, [$request->attributes->get('id')]);
        $user->setCart($cart);
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute("index");
    }

    public function getCartMovies()
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);
        $user = $this->getUser();
        $cart = $user->getCart();
        $userCart = [];
        foreach($cart as $key =>$value) {
            $movie = $repository->findBy(['id' => $value]);
            $userCart[$key]['id'] = $movie[0]->getId();
            $userCart[$key]['title'] = $movie[0]->getTitle();
            $userCart[$key]['price'] = $movie[0]->getPrice();
        }

        return $userCart;
    }

    public function cartSum(array $cart): float
    {
        $cost = [];
        foreach($cart as $movie) {
            $cost[] = $movie['price'];
        }

        return array_sum($cost);
    }

    public function moneyConverter(float $money): float
    {
        return round($money / 4.38, 2);
    }
}